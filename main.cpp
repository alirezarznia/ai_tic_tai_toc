

//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/
#include <bits/stdc++.h>


#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<pii , long long >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-10
#define 	inf 		        1ll<<62
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);


typedef long long ll;
typedef long double ld;

using namespace std;


ll pw(ll x, ll y){
    if(y==0) return 1;
    ll p = pw(x , y/2);
    if(y%2) return x*p*p;
    return p*p;
}
ll arr[3][3];
map<ll ,pii>mp;
map<pii ,ll>mpt;
bool CHECK_NOT_EMPTY(){
    Rep(i ,3){
        Rep(j ,3) if(!arr[i][j]) return false;
    }
    return true;
}

ll GET_POINT(){
    Rep(i ,3){
        if(arr[i][0]== arr[i][1] && arr[i][0] == arr[i][2] && arr[i][0]==1) return 20ll;
        if(arr[i][0]== arr[i][1] && arr[i][0] == arr[i][2] && arr[i][0]==2) return -20ll;
        if(arr[0][i]== arr[1][i] && arr[0][i] == arr[2][i] && arr[0][i]==2) return -20ll;
        if(arr[0][i]== arr[1][i] && arr[0][i] == arr[2][i] && arr[0][i]==1) return 20ll;
    }
    if(arr[0][0]==1 && arr[0][0]==arr[1][1] && arr[1][1]==arr[2][2]) return 20ll;
    if(arr[0][2]==1 && arr[0][2]==arr[1][1] && arr[1][1]==arr[2][0]) return 20ll;
    if(arr[0][2]==2 && arr[0][2]==arr[1][1] && arr[1][1]==arr[2][0]) return -20ll;
    if(arr[0][0]==2 && arr[0][0]==arr[1][1] && arr[1][1]==arr[2][2]) return -20ll;
    return 0;
}
ll FIND_OPT(ll dep , ll trn)
{
    if(CHECK_NOT_EMPTY()) return 0;
    ll point = GET_POINT();
    if(point) return point + trn*dep;
    if(trn==1)
    {
        ll mx =-inf;
        Rep(i ,3){
            Rep(j ,3){
                if(!arr[i][j]){
                    arr[i][j]=1;
                    mx=max(mx ,FIND_OPT(dep+1 , -1));
                    arr[i][j]=0;
                }
            }
        }
        return mx;
    }
    else
    {
            ll mn =inf;
        Rep(i ,3){
            Rep(j ,3){
                if(!arr[i][j]){
                    arr[i][j]=2;
                    mn=min(mn ,FIND_OPT(dep+1 , 1));
                    arr[i][j]=0;
                }
            }
        }
        return mn;
    }
}
ll CALL(){
    ll mx =-inf;
    pii cur ;
    Rep(i ,3){
        Rep(j ,3){
            if(!arr[i][j]){
                arr[i][j] = 1;
                ll ans = FIND_OPT(0 , -1);
                arr[i][j]=0;
                if(ans>mx) cur.F = i , cur.S = j , mx = ans;
            }
        }
    }
    return mpt[cur];
}
int main(){
  //  Test;
    mp[1] = MP(0 ,0) , mpt[MP(0 ,0)] =1;
    mp[2] = MP(0 ,1), mpt[MP(0 ,1)] =2;
    mp[3] = MP(0 ,2), mpt[MP(0 ,2)] =3;
    mp[4] = MP(1 ,0), mpt[MP(1 ,0)] =4;
    mp[5] = MP(1 ,1), mpt[MP(1 ,1)] =5;
    mp[6] = MP(1 ,2), mpt[MP(1 ,2)] =6;
    mp[7] = MP(2 ,0), mpt[MP(2 ,0)] =7;
    mp[8] = MP(2 ,1), mpt[MP(2 ,1)] =8;
    mp[9] = MP(2 ,2), mpt[MP(2 ,2)] =9;
    ll status;
    while(cin>>status){
        if(status == -1) break;
        ll X , O ,E;
        cin>>E;
        Rep(i ,E){
            ll x; cin>>x;
        }
        cin>>X;
        Rep(i ,X){
            ll x ;cin>>x;
            arr[mp[x].F][mp[x].S]=1;
        }
        cin>>O;
        Rep(i ,O){
            ll x ;cin>>x;
            arr[mp[x].F][mp[x].S]=2;
        }
        if(status != 1) continue;
        ll ans = CALL();
        cout<<ans<<endl;
    }
    return 0;
}

